import Text.Parsec.Char (oneOf)
import Text.Parsec (parse, skipMany1, space)

-- New Datatype
data Lex = Atom String
           | List [Lex]
           | Number Integer
           | String String
           | Bool Bool
           | Cons Lex Lex

symbol = oneOf "!+-*/<=>"
spaces = skipMany1 space

readExpr :: String -> String
readExpr input = case parse (spaces >> symbol) "scheme2-vgc" input of
  Left err ->  "No match:" ++ input ++ show err
  Right val -> "scheme2-vgc: " ++ show val

main = print $ readExpr " ++"
